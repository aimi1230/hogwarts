# @Author : Wei
# @File : animal.py
""""
创建一个类（Animal）【动物类】，类里有属性（名称，颜色，年龄，性别），类方法（会叫，会跑）
创建子类【猫】，继承【动物类】
重写父类的__init__方法，继承父类的属性
添加一个新的属性，毛发=短毛
添加一个新的方法， 会捉老鼠，
重写父类的‘【会叫】的方法，改成【喵喵叫】
创建子类【狗】，继承【动物类】
复写父类的__init__方法，继承父类的属性
添加一个新的属性，毛发=长毛
添加一个新的方法， 会看家
复写父类的【会叫】的方法，改成【汪汪叫】
"""


class Animal:
    def __init__(self, name, color, age, sex):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex

    def bark(self):
        print(f"动物叫的方法")

    def run(self):
        print(f"动物跑的方法")


class Cat(Animal):

    def __init__(self, name, color, age, sex):
        self.hair = 'short'
        super().__init__(name, color, age, sex)

    def catchmouse(self):
        print("小猫抓到了一只老鼠")

    def bark(self):
        super().bark()
        print("小猫在喵喵叫")


class Dog(Animal):
    def __init__(self, name, color, age, sex):
        self.hair = '长毛'
        super().__init__(name, color, age, sex)

    def protecthome(self):
        print(f"一只{dog.color}的{dog.hair}狗今年{dog.age}岁了,它的名字叫{dog.name}，它是{dog.sex}，一直在看家")


    def bark(self):
       # super().bark()
        print("狗在汪汪叫")


if __name__ == '__main__':
    cat = Cat('kk','Yellow','3','公猫')
    cat.catchmouse()
    cat.bark()
    print(f"猫的名字叫{cat.name},颜色是{cat.color},今年{cat.age}岁了,它是一只{cat.sex},毛是{cat.hair}的哟")

    dog = Dog('花花', '白色', '2', '公狗')
    dog.protecthome()
    dog.bark()


